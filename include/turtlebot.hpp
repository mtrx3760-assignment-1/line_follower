/** MIT License
Copyright (c) 2017 Sudarshan Raghunathan
file turtlebot.hpp
Class definition for turtlebot
*/
//Last updated by Jiazheng Liu to refactor the code to OOD

#pragma once
#include <geometry_msgs/Twist.h>
#include <vector>
#include "ros/ros.h"
#include "line_follower_turtlebot/pos.h"

/**
Class turtlebot subscribes to the directions published and publishes velocity commands
*/
class turtlebot {
 public:
    int dir;  /// Direction message to read published directions
/**
Callback used to subscribe to the direction message published by the Line detection node
msg is the custom message pos which publishes a direction int between 0 and 3
*/
    void dir_sub(line_follower_turtlebot::pos msg);
/**
Function to publish velocity commands based on direction
velocity is the twist 
pub is used to publish the velocity commands to the turtlebot
rate is the ros loop rate for publishing the commands
*/
    void vel_cmd(geometry_msgs::Twist &velocity,
     ros::Publisher &pub, ros::Rate &rate);
};
